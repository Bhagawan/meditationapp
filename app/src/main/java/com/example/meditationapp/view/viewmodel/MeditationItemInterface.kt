package com.example.meditationapp.view.viewmodel

interface MeditationItemInterface {
    fun click(file: String)
}