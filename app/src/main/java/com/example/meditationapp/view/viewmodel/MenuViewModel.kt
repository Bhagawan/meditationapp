package com.example.meditationapp.view.viewmodel

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.databinding.library.baseAdapters.BR
import androidx.navigation.findNavController
import com.example.meditationapp.data.ImageList
import com.example.meditationapp.internet.ImageClient
import com.example.meditationapp.view.ui.MenuFragmentDirections
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception
import java.lang.Integer.min
import kotlin.random.Random

@BindingAdapter("backBitmap")
fun setBack(v: View, bitmap: Bitmap?) {
    if(v.width > 0 && bitmap != null) v.background = BitmapDrawable(v.resources, Bitmap.createBitmap(bitmap,0,0, min(v.width, bitmap.width), min(v.height, bitmap.height)))
}

class MenuViewModel: BaseObservable() {
    private lateinit var target: Target

    @Bindable
    var background: Bitmap? = null

    init {
        loadBackground("http://195.201.125.8/MeditationApp/forest_two.png")
        ImageClient.create().getList("tree")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ImageList> {
                    lateinit var d: Disposable
                    override fun onNext(t: ImageList) {
                        loadBackground(t.list[Random.nextInt(t.list.size - 1)].path)
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        d.dispose()
                    }

                    override fun onComplete() {
                        d.dispose()
                    }

                    override fun onSubscribe(d: Disposable) {
                        this.d = d
                    }
                })
    }

    fun switchToRelax(v: View) {
        v.findNavController().navigate(MenuFragmentDirections.actionMenuFragmentToRelaxFragment())
    }

    fun switchToMeditation(v: View) {
        v.findNavController().navigate(MenuFragmentDirections.actionMenuFragmentToMeditationFragment())
    }

    fun setBack(back: Bitmap) {
        background = back
        notifyPropertyChanged(BR.background)
    }

    private fun loadBackground(url: String) {
        target = object : Target{
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { setBack(it) }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load(url).into(target)
    }
}