package com.example.meditationapp.view.viewmodel

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.navigation.findNavController
import com.example.meditationapp.data.ImageJson
import com.example.meditationapp.data.ImageList
import com.example.meditationapp.internet.ImageClient
import com.squareup.picasso.*
import com.squareup.picasso.Target
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class RelaxViewModel: BaseObservable() {
    private lateinit var target: Target
    private lateinit var images: List<ImageJson>
    private val player = MediaPlayer()

    @Bindable
    var background: Bitmap? = null

    init {
        ImageClient.create().getList("green forest")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ImageList> {
                lateinit var d: Disposable
                override fun onNext(t: ImageList) {
                    images = t.list
                    startDynamicBackground()
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    d.dispose()
                }

                override fun onComplete() {
                    d.dispose()
                }

                override fun onSubscribe(d: Disposable) {
                    this.d = d
                }
            })
        player.setDataSource( "http://195.201.125.8/MeditationApp/sounds/forest_sounds.mp3")
        player.setVolume(100.0f,100.0f)
        player.setOnPreparedListener {
                mp -> mp.start()
        }
        player.setOnCompletionListener {
            it.release()
        }
    }

    fun closeScreen(v: View) {
        if(player.isPlaying) {
            player.stop()
            player.release()
        }
        v.findNavController().popBackStack()
    }

    fun setBack(back: Bitmap) {
        background = back
        notifyPropertyChanged(BR.background)
    }

    private fun startDynamicBackground() {
        loadBackground("http://195.201.125.8/MeditationApp/forest_one.png")
        Observable.timer(5000, TimeUnit.MILLISECONDS)
            .repeat()
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                loadBackground(images[Random.nextInt(images.size - 1)].path)
            }
    }

    private fun loadBackground(url: String) {
        target = object : Target{
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { setBack(it) }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load(url).into(target)
    }
}