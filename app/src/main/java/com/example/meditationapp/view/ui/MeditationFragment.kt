package com.example.meditationapp.view.ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.meditationapp.databinding.FragmentMeditationBinding
import com.example.meditationapp.view.adapter.BindableAdapter
import com.example.meditationapp.view.adapter.MeditationsAdapter
import com.example.meditationapp.view.viewmodel.MeditationViewModel

@Suppress("UNCHECKED_CAST")
@BindingAdapter("recyclerData")
fun <T> setMeditations(v: RecyclerView, data: T) {
    if(v.adapter != null){
        if(v.adapter is BindableAdapter<*>) {
            (v.adapter as BindableAdapter<T>).setData(data)
        }
    }
}

class MeditationFragment : Fragment() {
    private lateinit var binding: FragmentMeditationBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                binding.viewModel?.closeScreen(binding.root)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentMeditationBinding.inflate(layoutInflater)
        binding.viewModel = MeditationViewModel()
        binding.executePendingBindings()
        fullscreen(true)

        binding.recyclerMeditations.layoutManager = LinearLayoutManager(context)
        binding.recyclerMeditations.adapter = MeditationsAdapter()

        return binding.root
    }

    private fun fullscreen(fullscreen: Boolean) {
        activity?.let {
            WindowInsetsControllerCompat(it.window, it.window.decorView).let { controller ->
                if(fullscreen) {
                    controller.hide(WindowInsetsCompat.Type.systemBars())
                    controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    WindowCompat.setDecorFitsSystemWindows(it.window, false)
                } else {
                    controller.show(WindowInsetsCompat.Type.systemBars())
                    controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    WindowCompat.setDecorFitsSystemWindows(it.window, true)
                }
            }
        }
    }



    override fun onDestroy() {
        fullscreen(false)
        super.onDestroy()
    }
}