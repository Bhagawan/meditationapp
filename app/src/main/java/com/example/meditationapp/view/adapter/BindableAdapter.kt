package com.example.meditationapp.view.adapter

interface BindableAdapter<T> {
    fun setData(data: T)
}