package com.example.meditationapp.view.viewmodel

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.media.*
import android.view.View
import android.widget.ToggleButton
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.databinding.library.baseAdapters.BR
import androidx.navigation.findNavController
import com.example.meditationapp.data.ImageJson
import com.example.meditationapp.data.ImageList
import com.example.meditationapp.data.Meditation
import com.example.meditationapp.internet.ImageClient
import com.example.meditationapp.internet.ServerClient
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import kotlin.random.Random

@BindingAdapter("state")
fun setState(v: ToggleButton, state: Boolean) {
    v.isChecked = state
}

class MeditationViewModel: BaseObservable() {
    private lateinit var target: Target
    private lateinit var images: List<ImageJson>
    private var player: MediaPlayer = MediaPlayer()
    private var playerWasReleased = false
    private val progressUpdater = Observable
        .timer(100, TimeUnit.MILLISECONDS)
        .repeat()
        .observeOn(AndroidSchedulers.mainThread())
    private lateinit var progressObserver: Disposable

    @Bindable
    var meditations = emptyList<MeditationItemViewModel>()
    @Bindable
    var playerVisibility = View.GONE
    @Bindable
    var recyclerVisibility = View.VISIBLE
    @Bindable
    var background: Bitmap? = null
    @Bindable
    var meditationPercent = 0
    @Bindable
    var playState = false

    init {
        ImageClient.create().getList("mountain")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ImageList> {
                lateinit var d: Disposable
                override fun onNext(t: ImageList) {
                    images = t.list
                    startDynamicBackground()
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    d.dispose()
                }

                override fun onComplete() {
                    d.dispose()
                }

                override fun onSubscribe(d: Disposable) {
                    this.d = d
                }
            })
        ServerClient.create().getMeditations()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                updateMeditationList(it)
            }
    }

    fun closeScreen(v: View) {
        if(!playerWasReleased){
            if(player.isPlaying) {
                player.stop()
                player.release()
            }
        }
        v.findNavController().popBackStack()
    }

    fun setBack(back: Bitmap) {
        background = back
        notifyPropertyChanged(BR.background)
    }

    fun backClick(v: View) {
        if(playerVisibility == View.VISIBLE) {
            playerVisibility = View.GONE
            recyclerVisibility = View.VISIBLE
            notifyPropertyChanged(BR.playerVisibility)
            notifyPropertyChanged(BR.recyclerVisibility)
            progressObserver.dispose()
            if(player.isPlaying) {
                player.stop()
                player.release()
                playerWasReleased = true
            }
            playState = false
            notifyPropertyChanged(BR.playState)
        } else closeScreen(v)
    }

    fun playClick(v: View) {
        if(player.isPlaying) player.pause()
        else player.start()
        playState = player.isPlaying
    }

    private fun updateMeditationList(meditations: List<Meditation>) {
        val newList = ArrayList<MeditationItemViewModel>()
        for(meditation in meditations) {
            newList.add(MeditationItemViewModel(meditation, object: MeditationItemInterface {
                override fun click(file: String) {
                    startMeditation(meditation.file)
                }
            }))
        }
        if(newList.isNotEmpty()) {
            this.meditations = newList
            notifyPropertyChanged(BR.meditations)
        }
    }

    private fun startMeditation(file: String) {
        player = MediaPlayer()
        player.setDataSource( "http://195.201.125.8/MeditationApp/sounds/$file")
        player.setVolume(100.0f,100.0f)
        player.setOnPreparedListener { mp ->
            playerWasReleased = false
            mp.start()
            startDynamicPercent()
            playerVisibility = View.VISIBLE
            recyclerVisibility = View.GONE
            notifyPropertyChanged(BR.playerVisibility)
            notifyPropertyChanged(BR.recyclerVisibility)
        }
        player.prepareAsync()
    }

    private fun startDynamicPercent() {
        progressObserver = progressUpdater.repeat().subscribe {
                if(player.isPlaying) {
                    meditationPercent = player.currentPosition * 100 / player.duration
                    notifyPropertyChanged(BR.meditationPercent)
                }
            }
    }

    private fun startDynamicBackground() {
        loadBackground("http://195.201.125.8/MeditationApp/montauin.png")
        Observable.timer(5000, TimeUnit.MILLISECONDS)
            .repeat()
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                loadBackground(images[Random.nextInt(images.size - 1)].path)
            }
    }

    private fun loadBackground(url: String) {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { setBack(it) }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load(url).into(target)
    }
}