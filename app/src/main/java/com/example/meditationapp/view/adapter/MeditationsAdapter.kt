package com.example.meditationapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.meditationapp.R
import com.example.meditationapp.view.viewmodel.MeditationItemViewModel
import androidx.databinding.library.baseAdapters.BR

class MeditationsAdapter: RecyclerView.Adapter<MeditationsAdapter.ViewHolder>(), BindableAdapter<List<MeditationItemViewModel>> {
    private var meditations = emptyList<MeditationItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_meditation, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(meditations[position])
    }

    override fun getItemCount(): Int {
        return meditations.size
    }

    override fun setData(data: List<MeditationItemViewModel>) {
        meditations = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: MeditationItemViewModel) {
            binding.setVariable(BR.viewModel, viewModel)
        }
    }
}