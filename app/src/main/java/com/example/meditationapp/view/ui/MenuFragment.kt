package com.example.meditationapp.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.meditationapp.databinding.FragmentMenuBinding
import com.example.meditationapp.view.viewmodel.MenuViewModel

class MenuFragment : Fragment() {
    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentMenuBinding.inflate(layoutInflater)
        binding.viewModel = MenuViewModel()
        binding.executePendingBindings()

        return binding.root
    }

}