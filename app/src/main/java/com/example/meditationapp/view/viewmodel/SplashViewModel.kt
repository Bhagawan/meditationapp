package com.example.meditationapp.view.viewmodel

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.View
import android.webkit.WebView
import android.widget.ImageView
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import com.example.meditationapp.data.SplashResponse
import com.example.meditationapp.internet.ServerClient
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import androidx.databinding.library.baseAdapters.BR
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.reactivex.subjects.PublishSubject
import java.util.*

@BindingAdapter("url")
fun setUrl(v: WebView, url: String) {
    v.loadUrl(url)
}

@BindingAdapter("image")
fun setImage(v: ImageView, bitmap: Bitmap?) {
    bitmap?.let { v.setImageBitmap(it) }
}

class SplashViewModel: BaseObservable() {
    private lateinit var target: Target
    private var requestInProcess = false

    var switchToMain: PublishSubject<Boolean> = PublishSubject.create()

    @Bindable
    var url  = ""
    @Bindable
    var logo: Bitmap? = null
    @Bindable
    var webViewVisibility = View.VISIBLE
    @Bindable
    var logoVisibility = View.GONE

    init {
        showLogo()
        ServerClient.create().getSplash(Locale.getDefault().language)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<SplashResponse> {
                lateinit var d: Disposable
                override fun onNext(t: SplashResponse) {
                    if (requestInProcess) {
                        requestInProcess = false
                        hideLogo()
                    }
                    if(t.url != "no") {
                        url = "https://${t.url}"
                        notifyPropertyChanged(BR.url)
                    } else switchToMain.onNext(true)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    switchToMain.onNext(true)
                    d.dispose()
                }

                override fun onComplete() {
                    d.dispose()
                }

                override fun onSubscribe(d: Disposable) {
                    this.d = d
                }
            })
    }

    private fun showLogo() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { logo = it
                    notifyPropertyChanged(BR.logo)
                    if(requestInProcess) {
                        webViewVisibility = View.GONE
                        notifyPropertyChanged(BR.webViewVisibility)
                        logoVisibility = View.VISIBLE
                        notifyPropertyChanged(BR.logoVisibility)
                    }
                }
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        requestInProcess = true
        Picasso.get().load("http://195.201.125.8/MeditationApp/logo.png").into(target)
    }

    fun hideLogo() {
        webViewVisibility = View.VISIBLE
        notifyPropertyChanged(BR.webViewVisibility)
        logoVisibility = View.GONE
        notifyPropertyChanged(BR.logoVisibility)
    }
}