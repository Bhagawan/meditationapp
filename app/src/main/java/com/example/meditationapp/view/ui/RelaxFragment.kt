package com.example.meditationapp.view.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import com.example.meditationapp.databinding.FragmentRelaxBinding
import com.example.meditationapp.view.viewmodel.RelaxViewModel

class RelaxFragment : Fragment() {
    lateinit var binding: FragmentRelaxBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                binding.viewModel?.closeScreen(binding.root)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentRelaxBinding.inflate(layoutInflater)
        binding.viewModel = RelaxViewModel()
        binding.executePendingBindings()
        fullscreen(true)

        return binding.root
    }

    private fun fullscreen(fullscreen: Boolean) {
        activity?.let {
            WindowInsetsControllerCompat(it.window, it.window.decorView).let { controller ->
                if(fullscreen) {
                    controller.hide(WindowInsetsCompat.Type.systemBars())
                    controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    WindowCompat.setDecorFitsSystemWindows(it.window, false)
                } else {
                    controller.show(WindowInsetsCompat.Type.systemBars())
                    controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    WindowCompat.setDecorFitsSystemWindows(it.window, true)
                }
            }
        }
    }

    override fun onDestroy() {
        fullscreen(false)
        super.onDestroy()
    }
}