package com.example.meditationapp.data

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

