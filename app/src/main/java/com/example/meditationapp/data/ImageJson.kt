package com.example.meditationapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ImageJson(@SerializedName("path") val path: String)
