package com.example.meditationapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Meditation(@SerializedName("name") val name: String, @SerializedName("file") val file: String)
