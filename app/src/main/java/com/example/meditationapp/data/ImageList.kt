package com.example.meditationapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ImageList(@SerializedName("data") val list:List<ImageJson>)
