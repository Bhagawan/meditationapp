package com.example.meditationapp

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.meditationapp.databinding.ActivitySplashBinding
import com.example.meditationapp.util.MyWebChromeClient
import com.example.meditationapp.view.viewmodel.SplashViewModel

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity(){
    private lateinit var binding: ActivitySplashBinding

    override fun onBackPressed() {
        if (binding.webViewSplash.canGoBack()) {
            binding.webViewSplash.goBack()
            return
        } else finish()
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        binding.viewModel = SplashViewModel()
        (binding.viewModel as SplashViewModel).switchToMain.subscribe { if(it) switchToMain() }
        initialiseWebView()

        setContentView(binding.root)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    private fun switchToMain() {
        val i = Intent(applicationContext, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(i)
        finish()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initialiseWebView() {
        binding.webViewSplash.webViewClient = WebViewClient()
        binding.webViewSplash.webChromeClient = MyWebChromeClient(this, binding.webViewSplash, binding.webViewCustomView)
        val webSettings = binding.webViewSplash.settings
        binding.webViewSplash.scrollBarStyle = View.SCROLLBARS_OUTSIDE_OVERLAY
        val cookie = CookieManager.getInstance()
        cookie.setAcceptCookie(true)
        binding.webViewSplash.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.allowContentAccess = true
        webSettings.allowFileAccessFromFileURLs = true
        webSettings.allowUniversalAccessFromFileURLs = true
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.allowFileAccess = true
        webSettings.domStorageEnabled = true
        webSettings.databaseEnabled = true
        webSettings.useWideViewPort = true
        webSettings.supportZoom()
        webSettings.setAppCacheEnabled(true)
        webSettings.userAgentString = webSettings.userAgentString.replace("; wv", "")
    }
}
