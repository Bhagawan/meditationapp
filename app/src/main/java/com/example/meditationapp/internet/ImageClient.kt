package com.example.meditationapp.internet

import com.example.meditationapp.data.ImageList
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import retrofit2.http.Query

interface ImageClient {

    @POST("search")
    fun getList(@Query("q") query: String): Observable<ImageList>

    companion object {
        fun create() : ImageClient {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.newThread()))
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://wallhaven.cc/api/v1/")
                .build()
            return retrofit.create(ImageClient::class.java)
        }
    }
}