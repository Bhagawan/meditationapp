package com.example.meditationapp.internet

import com.example.meditationapp.data.Meditation
import com.example.meditationapp.data.SplashResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ServerClient {

    @FormUrlEncoded
    @POST("MeditationApp/splash.php")
    fun getSplash(@Field("locale") locale: String): Observable<SplashResponse>

    @GET("MeditationApp/meditations.json")
    fun getMeditations(): Observable<List<Meditation>>

    companion object {
        fun create() : ServerClient {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.newThread()))
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClient::class.java)
        }
    }
}